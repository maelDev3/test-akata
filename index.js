//verifie html video ou non

//Premier Problème verifie balise html ferme ou ouvert ou bien endroit
function isValidHtml(html) {
  const Tableau = []; 
  //verifie le caractere minuscule et majuscule jusque a et Z
  const verifie_caractere = /<(\/?[a-zA-Z]+)>/g;

  let check;
  //function exec Vérifier si une chaîne de caractères HTML contient des caractères spéciaux.
  //boucle while pour parcourir toutes les balises dans la chaîne html
  while ((check = verifie_caractere.exec(html)) !== null) {
    const info = check[1]; 
    if (info.startsWith('</')) {  // La function startsWith() retourne true si la chaîne de caractères commence par la sous-chaîne spécifiée (en l'occurrence "</", dans ce cas)
       // Stocker l'iformation une balise fermante
      //function Pop() permet de supprimer le dernier élément du tableau et de le renvoyer
      const last_info = Tableau.pop();  
      //function subrtring substring() renvoie une nouvelle chaîne de caractères 
      if (!last_info || last_info !== info.substring(1)) { 
        return false;
      }
    } else { 
      // Stocker l'iformation une balise ouvrante
      Tableau.push(info); 
    }
  }

  return Tableau.length === 0; 
}




//Dexième probleme verifie le developpeur plus année d'experience
function pairProgramming(experiences, isMostExperienced) {
	//parcourir experience, Trier les objets par ordre croissant d'expérience
  const filtreExperience = experiences.map((item, index) => ({ item, index })) * .sort((a, b) => a.item - b.item); 

 //verifie si plus experience ou non
  const list_index = isMostExperienced
    ? [filtreExperience.length - 1, filtreExperience.length - 2] 
    : [0, 1]; 
  return list_index.map(index => filtreExperience[index].index);
}



//Troisième problème
//Ecrire dans un fichier 3.sql une requête SQL(MySQL ou PostgreSQL selon votre choix) 
SELECT niveau FROM resultat_examen WHERE code_matiere = 'ALG' AND note >= 10 GROUP BY niveau HAVING COUNT(*) >= 30;

